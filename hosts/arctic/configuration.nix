{ lib, pkgs, systemSettings, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ../shared
    ../../system/hardware/nvidia.nix
    ../../system/hardware/printing.nix
    ../../system/hardware/bluetooth.nix
    ../../system/desktop/hyprland.nix
    ../../system/services/mpd.nix
    ../../system/misc/fing.nix
    ../../system/virtualisation
  ];

  networking.hostName = systemSettings.hostname;

  boot.plymouth = {
    enable = true;
  };
  boot.loader = {
    systemd-boot.enable = lib.mkForce false;
    timeout = 60;
    grub = {
      enable = true;
      efiSupport = true;
      devices = [ "nodev" ];
      useOSProber = true;
      theme = pkgs.stdenv.mkDerivation {
        pname = "distro-grub-themes";
        version = "3.1";
        src = pkgs.fetchFromGitHub {
          owner = "AdisonCavani";
          repo = "distro-grub-themes";
          rev = "v3.1";
          hash = "sha256-ZcoGbbOMDDwjLhsvs77C7G7vINQnprdfI37a9ccrmPs=";
        };
        installPhase = "cp -r customize/nixos $out";
      };
    };
  };

  nix = {
    settings.experimental-features = [ "nix-command" "flakes" ];
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  system.stateVersion = "23.11";

}

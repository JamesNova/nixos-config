{
  description = "A very basic flake";

  outputs = { self, nixpkgs, ... }@inputs:
  let
    # -- System Settings -- #
    systemSettings = {
      hostname = "arctic";
      host = "arctic"; # only arctic for now
      system = "x86_64-linux";
      locale = "es_UY.UTF-8";
      timezone = "America/Montevideo";
    };

    # -- User Settings -- #
    userSettings = {
      name = "Tiago";
      username = "tiago"; # intial user password is same as username, change it with passw
      email = "tiagocalerom@gmail.com";
      gitName = "tiagocalero";
      gitEmail = "tcalerom@proton.me";
      profile = "penguin"; # only penguin for now
      theme = "dracula";
      browser = "firefox";
      term = "kitty";
      editor = "nvim";
      font = "JetBrains Mono Nerd";
      fontPkg = (nixpkgs.nerdfonts.override { fonts = ["JetBrainsMono"]; });
    };

    # configure pkgs
    pkgs = import nixpkgs {
      system = systemSettings.system;
      config.allowUnfree = true;
    };

    # configure lib
	  lib = nixpkgs.lib;
  in
  {

    nixosConfigurations = {
      arctic = lib.nixosSystem {
        specialArgs = { inherit inputs systemSettings userSettings self; };
        modules = [
          ./hosts/arctic/configuration.nix
        ];
  	};
    };

	  homeConfigurations = {
      penguin = inputs.home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.x86_64-linux;
        extraSpecialArgs = { inherit inputs userSettings self; };
        modules	= [ ./profiles/penguin/home.nix ];
		};
	  };
	};

  inputs = {

    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-colors.url = "github:misterio77/nix-colors";

  };
}

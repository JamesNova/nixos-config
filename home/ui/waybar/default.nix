{ config, ...}:
let
  c = config.colorScheme.palette;
in
{
  home.file.".config/waybar/restart.sh".text = ''
    #!/usr/bin/env bash
    pkill waybar && waybar &
  '';
  programs.waybar = {
  	enable = false;
    settings = [{
      layer = "top";
      position = "top";

      modules-left = ["custom/os"];
      modules-center = ["hyprland/workspaces"];
      modules-right = ["mpd" "pulseaudio" "clock" "tray"];

      "custom/os" = {
        format = " {} ";
          exec = "echo ' '";
          intreval = "once";
      };

        "hyprland/workspaces" = {
          all-outputs = true;
          format = "{name}";
          format-icons = {
            urgent = " ";
            focused = " ";
            default = " ";
          };
          on-scroll-up = "hyprctl dispatch workspace e+1";
          on-scroll-down = "hyprctl dispatch workspace e-1";
        };

        "tray" = {
          spacing = 12;
        };

        "clock" = {
          tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
          format-alt = "{:%Y-%m-%d}";
        };

        "pulseaudio" = {
          format = "{icon}  {volume}%";
          format-bluetooth = "{volume}% {icon} {format_source}";
          format-bluetooth-muted = " {icon} {format_source}";
          format-muted = " {format_source}";
          format-icons = {
            headphone = "";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = ["" "" ""];
          };
          on-click = "pavucontrol";
        };

        "mpd" = {
          tooltip = false;
          format = "{stateIcon}";
          format-disconnected = "Disconnected ...";
          format-stopped = "Stopped";
          interval = 10;
          on-click = "mpc toggle";
          on-click-right = "kitty --class 'music' ncmpcpp";
          on-click-left = "kitty --class 'visual' cava";
          on-scroll-up = "mpc next";
          on-scroll-down = "mpc prev";
          state-icons = {
            playing = "";
            paused = "";
          };
        };
    }]; # end of Settings

    style = ''
    * {
        font-family: FontAwesome, Material Design, Roboto, Helvetica, Arial, sans-serif;
        font-size: 14px;
    }

    window#waybar {
        background-color: #${c.base00};
        color: #${c.base05};
        transition-property: background-color;
        transition-duration: .5s;
    }

    #workspaces {
        margin: 5px;
        background: #${c.base01};
        border-radius: 15px;
        border: 0px;
    }

    #workspaces button {
        padding: 0px 5px;
        margin: 4px 3px;
        border-radius: 15px;
        border: 0px;
        color: #${c.base05};
        background: #${c.base01};
        opacity: 0.5;
        transition: all 0.3s ease-in-out;
    }

    #workspaces button.active {
        padding: 0px 5px;
        margin: 4px 3px;
        border-radius: 15px;
        border: 0px;
        color: #${c.base00};
        background: linear-gradient(135deg, #${c.base0D}, #${c.base09});
        opacity: 1.0;
        min-width: 40px;
        transition: all 0.3s ease-in-out;
    }

    #workspaces button:hover {
        border-radius: 15px;
        color: #${c.base00};
        background: linear-gradient(135deg, #${c.base0D}, #${c.base09});
        opacity: 0.7;
    }

    #custom-os {
        margin: 5px;
        font-size: 20px;
        color: #${c.base0D};
        background: #${c.base01};
        border-radius: 3px;
    }

    #mpd,
    #mpd.active {
        margin: 5px;
        padding: 0px 10px 0px 10px;
        color: #${c.base05};
        background: #${c.base01};
        border-radius: 3px;
    }

    #custom-notification,
    #clock,
    #pulseaudio,
    #tray {
        margin: 5px;
        padding: 0 10px;
        color: #${c.base05};
        background: #${c.base01};
        border-radius: 3px;
    }

    /* If workspaces is the leftmost module, omit left margin */
    .modules-left > widget:first-child > #workspaces {
        margin-left: 0;
    }

    /* If workspaces is the rightmost module, omit right margin */
    .modules-right > widget:last-child > #workspaces {
        margin-right: 0;
    }

    #tray > .passive {
        -gtk-icon-effect: dim;
    }

    #tray > .needs-attention {
        -gtk-icon-effect: highlight;
        background-color: #eb4d4b;
    }

    '';
  };
}

{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    pyprland
    qalculate-gtk
    xfce.thunar
    kitty
    swww
    wl-clipboard
  ];
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    extraConfig = with config.colorScheme.palette; ''
			monitor=DP-1,1920x1080@165,0x0,1

			$browser = firefox
			$terminal = kitty
			$menu = rofi -show drun -show-icons -theme "~/.config/rofi/launcher.rasi"

			env = XCURSOR_SIZE,24
			env = QT_QPA_PLATFORMTHEME,qt5ct

			input {
			    kb_layout = us
			    kb_variant =
			    kb_model =
			    kb_options =
			    kb_rules =
			    follow_mouse = 1
			    touchpad {
			        natural_scroll = no
			    }
			    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
			}

			general {
			    gaps_in = 5
			    gaps_out = 20
			    border_size = 2
			    col.active_border = rgba(${base0D}ee) rgba(${base09}ee) 45deg
			    col.inactive_border = rgba(${base01}aa)
			    layout = dwindle
			    allow_tearing = false
			}

			decoration {
			    rounding = 10
			    blur {
			        enabled = true
			        size = 8
			        passes = 2
			    }
			    drop_shadow = yes
			    shadow_range = 8
			    shadow_render_power = 2
			    col.shadow = rgba(00000044)
			}

			animations {
			    enabled = yes
			    bezier = md3_standard, 0.2, 0, 0, 1
			    bezier = md3_decel, 0.05, 0.7, 0.1, 1
			    bezier = md3_accel, 0.3, 0, 0.8, 0.15
			    bezier = overshot, 0.05, 0.9, 0.1, 1.1
			    bezier = crazyshot, 0.1, 1.5, 0.76, 0.92
			    bezier = hyprnostretch, 0.05, 0.9, 0.1, 1.0
			    bezier = fluent_decel, 0.1, 1, 0, 1
			    # Animation configs
			    animation = windows, 1, 2, md3_decel, slide
			    animation = windowsOut, 1, 7, default, popin
			    animation = border, 1, 10, default
			    animation = fade, 1, 2, default
			    animation = workspaces, 1, 3, md3_decel
			    animation = specialWorkspace, 1, 3, md3_decel, slidevert
			}

			dwindle {
			    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
			    pseudotile = yes
			    preserve_split = yes
			}

			master {
			    new_status = master
			}

			gestures {
			    workspace_swipe = off
			}

			misc {
			    force_default_wallpaper = -1
			}

			device {
          name = epic-mouse-v1
			    sensitivity = -0.5
			}

			windowrulev2 = suppressevent maximize, class:.*
			windowrulev2 = allowsinput, class:.*

			$mainMod = SUPER

			bind = $mainMod, RETURN, exec, $terminal
			bind = $mainMod SHIFT, C, killactive,
			bind = $mainMod SHIFT, Q,  exit,
			bind = $mainMod, B, exec, $browser
			bind = $mainMod, SPACE, exec, $menu
			bind = $mainMod, P, pseudo, # dwindle
			bind = $mainMod, J, togglesplit, # dwindle
			bind = $mainMod SHIFT, F, togglefloating,
			bind = $mainMod, F, fullscreen,

			# Update Waybar config
			bind = $mainMod SHIFT, W, exec, ~/.config/waybar/restart.sh

			# Change window focus
			bind = $mainMod, m, movefocus, l
			bind = $mainMod, i, movefocus, r
			bind = $mainMod, e, movefocus, u
			bind = $mainMod, n, movefocus, d

			# Swap active window
			bind = $mainMod SHIFT, m, swapwindow, l
			bind = $mainMod SHIFT, i, swapwindow, r
			bind = $mainMod SHIFT, e, swapwindow, u
			bind = $mainMod SHIFT, n, swapwindow, d

			# Resize active window
			bind = $mainMod, LEFT, resizeactive, -40 0
			bind = $mainMod, RIGHT, resizeactive, 40 0
			bind = $mainMod, UP, resizeactive, 0 -40
			bind = $mainMod, DOWN, resizeactive, 0 40

			# Go to workspace
			bind = $mainMod, 1, workspace, 1
			bind = $mainMod, 2, workspace, 2
			bind = $mainMod, 3, workspace, 3
			bind = $mainMod, 4, workspace, 4
			bind = $mainMod, 5, workspace, 5
			bind = $mainMod, 6, workspace, 6
			bind = $mainMod, 7, workspace, 7
			bind = $mainMod, 8, workspace, 8
			bind = $mainMod, 9, workspace, 9
			bind = $mainMod, 0, workspace, 10

			# Move active window to workspace
			bind = $mainMod SHIFT, 1, movetoworkspace, 1
			bind = $mainMod SHIFT, 2, movetoworkspace, 2
			bind = $mainMod SHIFT, 3, movetoworkspace, 3
			bind = $mainMod SHIFT, 4, movetoworkspace, 4
			bind = $mainMod SHIFT, 5, movetoworkspace, 5
			bind = $mainMod SHIFT, 6, movetoworkspace, 6
			bind = $mainMod SHIFT, 7, movetoworkspace, 7
			bind = $mainMod SHIFT, 8, movetoworkspace, 8
			bind = $mainMod SHIFT, 9, movetoworkspace, 9
			bind = $mainMod SHIFT, 0, movetoworkspace, 10

			# Switch between current and last workspaces
			binds {
			    workspace_back_and_forth = true
			    allow_workspace_cycles = true
			}
			bind = $mainMod, SLASH, workspace, previous

			# Scratchpads
			bind = $mainMod, S, togglespecialworkspace, magic
			bind = $mainMod SHIFT, S, movetoworkspace, special:magic
			bind = $mainMod, K, exec, pypr toggle term && hyprctl dispatch bringactivetop
			bind = $mainMod, C, exec, pypr toggle calc && hyprctl dispatch bringactivetop
			bind = $mainMod, T, exec, pypr toggle thunar && hyprctl dispatch bringactivetop

			# Scroll through existing workspaces
			bind = $mainMod, mouse_down, workspace, e+1
			bind = $mainMod, mouse_up, workspace, e-1

			# Move/resize windows with mouse
			bindm = $mainMod, mouse:272, movewindow
			bindm = $mainMod, mouse:273, resizewindow

			# Window Rules
			$scratchpad = class:^(scratchpad)$
			$scratchpadsize = size 50% 55%
			windowrule = workspace 2, firefox
			windowrulev2 = float, $scratchpad
			windowrulev2 = $scratchpadsize, $scratchpad
			windowrulev2 = float, title:^(calculator)$
			windowrule = float, thunar
			windowrule = $scratchpadsize, thunar

      # Starting Programs
      exec-once = swww init &
      exec-once = nm-applet --indicator &
      #exec-once = waybar &
      exec-once = hyprctl setcursor Bibata-Modern-Classic 24 &
      exec = dunst &
      exec = pypr &
      exec = sleep 2
      exec = swww img /home/tiago/Downloads/wall2.jpg
    '';
  };
}

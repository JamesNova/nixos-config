{ ... }:
{
  programs.zsh = {
    enable = true;
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    enableCompletion = true;
    shellAliases = {
      v = "nvim";
      nix-pkgs = "nix --extra-experimental-features 'nix-command flakes' search nixpkgs";

      ls = "eza -1a --icons --color=always --group-directories-first";
      lg = "eza -la --icons --color=always --group-directories-first --no-permissions --no-filesize --no-time";
      la = "eza -la --icons --color=always --group-directories-first";
      ll = "eza -l --icons --color=always --group-directories-first";
      lt = "eza -aT --icons --color=always --group-directories-first";
      man = "sudo man";
      chown = "sudo chown";
      chmod = "sudo chmod";

      grep = "grep --color=auto";
      egrep = "egrep --color=auto";
      fgrep = "fgrep --color=auto";

      cp = "cp -ir";
      mv = "mv -i";
      rm = "rm -irf";
      cl = "clear";

      cd = "z";
      ".." = "cd ..";
      "..." = "cd ../..";
      "...." = "cd ../../..";

      # GIT
      ginit = "git init --initial-branch=master";
      origin = "git remote add origin";
      addup = "git add";
      addall = "git add .";
      branch = "git branch";
      checkout = "git checkout";
      clone = "git clone";
      commit = "git commit -m";
      fetch = "git fetch";
      pull = "git pull origin";
      push = "git push -u origin";
      stat = "git status";
      tag = "git tag";
      newtag = "git tag -a";
      gitrmc = "git rm -r --cached";
      gitrm = "git rm -r";
    };
    history = {
      expireDuplicatesFirst = true;
      save = 512;
    };
    initExtra = ''
    	export PATH="$HOME/.local/bin:$PATH"
      unsetopt beep
    '';
  };

  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      add_newline = false;
      format = "$directory$git_branch$git_status$cmd_duration\n[ ](fg:blue)  ";
      git_branch.format = "via [$symbol$branch(:$remote_branch)]($style) ";
      command_timeout = 1000;
    };
  };
}

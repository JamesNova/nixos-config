{ config, lib, ... }:
{
  programs.nixvim = {
    globals = {
      mapleader = " ";
      maplocalleader = " ";
  };

    keymaps = let
      normal =
        lib.mapAttrsToList
        (key: action: {
          mode = "n";
          inherit action key;
        })
        {
          # Esc to clear search results
          "<esc>" = ":noh<CR>";
          Q = "<nop>";

          # back and fourth between the two most recent files
          "<C-c>" = ":b#<CR>";

          # close by Ctrl+x and save by Space+s
          "<C-x>" = ":q<CR>";
          "<leader>s" = ":w<CR>";

          I = "$";
          M = "^";

          m = "h";
          n = "j";
          e = "k";
          i = "l";

          l = "i";
          L = "I";
          j = "e";
          J = "E";

          "<leader>m" = "<C-w>h";
          "<leader>i" = "<C-w>l";

          "-" = ":vertical resize +5<CR>";
          "=" = ":vertical resize -5<CR>";
          "+" = ":horizontal resize +2<CR>";
          "_" = ":horizontal resize -2<CR>";

          "<C-t>e" = ":bn<CR>";
          "<C-t>n" = ":bp<CR>";
          "<C-t>d" = ":bd<CR>";

          N = "mzJ`z";
          "<C-d>" = "<C-d>zz";
          "<C-u>" = "<C-u>zz";
          k = "nzzzv";
          K = "Nzzzv";

          "<leader>y" = ''"+y'';
          "<leader>Y" = ''"+Y'';

          "<leader>x" = ":!chmod +x %<CR>";

          #Tagbar
          "<C-g>" = ":TagbarToggle<CR>";

          #UndoTree
          "<leader>u" = ":UndotreeToggle<CR>";

          #NeoTree
          "<leader>t" = ":Neotree action=focus reveal toggle<CR>";
        };
      visual =
        lib.mapAttrsToList
        (key: action: {
          mode = "v";
          inherit action key;
        })
        {
          # better indenting
          ">" = ">gv";
          "<" = "<gv";
          "<TAB>" = ">gv";
          "<S-TAB>" = "<gv";

          m = "h";
          n = "j";
          e = "k";
          i = "l";

          N = ":m '>+1<CR>gv=gv";
          E = ":m '<-2<CR>gv=gv";

        };
    in
      config.lib.nixvim.keymaps.mkKeymaps
      {options.silent = true;}
      (normal ++ visual);
  };

}

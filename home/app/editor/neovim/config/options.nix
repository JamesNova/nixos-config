{ userSettings, ... }:
{
  programs.nixvim = {

    globals = {
	  mapleader = " ";
	  maplocalleader = " ";
	  loaded_netrw = 1;
	  loaded_netrwPlugin = 1;
	};

    autoCmd = [
    # Set indentation to 2 spaces for nix files
      {
        event = "FileType";
        pattern = "nix";
        command = "setlocal tabstop=2 shiftwidth=2";
      }

    ];

    opts = {
      fileencoding = "utf-8";

      nu = true;
      relativenumber = true;

      tabstop = 4;
      softtabstop = 4;
      shiftwidth = 4;
      expandtab = true;

      smartindent = true;
      wrap = true;

      swapfile = false;
      backup = false;
      undodir = "/home/${userSettings.username}/.vim/undodir";
      undofile = true;

      hlsearch = false;
      incsearch = true;

      termguicolors = true;

      scrolloff = 8;
      signcolumn = "yes";

      updatetime = 50;
      colorcolumn = "80";
    };
  };
}

{ ... }:
{
  imports = [
    ./config/options.nix
    ./config/keymaps.nix
    ./config/theme.nix
    ./plugins
  ];
  programs.nixvim = {
    enable = true;
    defaultEditor = true;
    highlight.ExtraWhitespace.bg = "red";
    highlightOverride = {
      Normal.bg = "NONE";
      Normal.ctermbg = "NONE";
    };
    match.ExtraWhitespace = "\\s\\+$";
  };

}

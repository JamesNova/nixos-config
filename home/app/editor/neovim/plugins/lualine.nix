{
  programs.nixvim.plugins.lualine = {
    enable = true;


    # +-------------------------------------------------+
    # | A | B | C                             X | Y | Z |
    # +-------------------------------------------------+
    settings = {
      options = {
        globalstatus = true;
        component_separators = {
          left = "";
          right = "";
        };
        section_separators = {
          left = "";
          right = "";
        };
      };
      sections = {
        lualine_a = ["mode"];
        lualine_b = ["filename"];
        lualine_c = ["branch" "diff" "diagnostics"];

        lualine_x = [
          # Show active language server
          # {
          #   name.__raw = ''
          #     function()
          #         local msg = ""
          #         local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
          #         local clients = vim.lsp.get_active_clients()
          #         if next(clients) == nil then
          #             return msg
          #         end
          #         for _, client in ipairs(clients) do
          #             local filetypes = client.config.filetypes
          #             if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
          #                 return client.name
          #             end
          #         end
          #         return msg
          #     end
          #   '';
          #   icon = "";
          #   color.fg = "#ffffff";
          # }
          "filetype"
        ];
        lualine_y = ["progress"];
        lualine_z = ["location"];
      };
      inactive_sections = {
        lualine_c = ["filename"];
        lualine_x = ["location"];
      };
    };
  };
}

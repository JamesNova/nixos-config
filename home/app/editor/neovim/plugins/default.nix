{
  programs.nixvim = {
    plugins = {
      oil.enable = true;
      nvim-colorizer = {
        enable = true;
        userDefaultOptions.names = false;
      };
    };
  };


  imports = [
    ./startify.nix
		./bufferline.nix
		./floaterm.nix
		./harpoon.nix
		./lsp.nix
		./lualine.nix
		./markdown-preview.nix
		./neo-tree.nix
		./tagbar.nix
		./telescope.nix
		./treesitter.nix
		./undotree.nix
    ./comment.nix
  ];
}

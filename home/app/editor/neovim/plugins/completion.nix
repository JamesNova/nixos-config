{
  programs.nixvim.plugins.nvim-cmp = {
    enable = true;
    autoEnableSources = true;
    sources = [
      {name = "nixd";}
      {name = "nvim_lsp";}
      {name = "path";}
      {name = "buffer";}
    ];
  };
}

{
  programs.nixvim.plugins = {
    treesitter = {
      enable = true;
      nixvimInjections = true;
      settings.indent.enable = true;
    };
    treesitter-refactor = {
      enable = true;
      smartRename.enable = true;
      highlightDefinitions = {
        enable = true;
        clearOnCursorMove = false;
      };
      navigation.enable = true;
      navigation.keymaps = {
        gotoNextUsage = "gnn";
        gotoPreviousUsage = "gnp";
      };
    };
  };
}

{
  programs.nixvim.plugins.lsp = {
    enable = true;
    servers = {
      lua-ls.enable = true;
      bashls.enable = true;
      nixd.enable = true;
      clangd.enable = true;
    };
  };
}

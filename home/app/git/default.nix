{ pkgs, config, userSettings, ... }:
{
    programs.git = {
      enable = true;
      userName = userSettings.gitName;
      userEmail = userSettings.gitEmail;
      aliases = {
        pu = "push";
      };
    };
}

{ pkgs, config, userSettings, ... }:
{
  services.mpd = {
    enable = true;
    musicDirectory = "${config.users.users.${userSettings.username}.home}/Music";
    dataDir = "${config.users.users.${userSettings.username}.home}/.config/mpd";
    user = "${userSettings.username}";
    startWhenNeeded = true;
    extraConfig = ''
      auto_update           "yes"
      restore_paused        "yes"
      audio_output {
        type "pipewire"
        name "Pipewire"
      }
      audio_output {
      	type                "fifo"
      	name                "Visualizer"
      	format              "44100:16:2"
      	path                "/tmp/mpd.fifo"
      }
    '';
  };
  environment.defaultPackages = [ pkgs.mpc-cli ];
  systemd.services.mpd.environment = {
    XDG_RUNTIME_DIR = "/run/user/1000";
  };
}

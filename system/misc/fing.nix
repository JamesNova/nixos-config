{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    gcc
    gnumake
    valgrind
    perl
  ];
}

{ pkgs, ... }:
{
  imports = [ ./pipewire.nix
              ./gnome-keyring.nix
              ./fonts.nix
            ];

  environment.systemPackages = [ pkgs.wayland ];

  # Configure xwayland
  services.xserver = {
    enable = true;
    xkb.layout = "us";
    xkb.variant = "";
    displayManager.gdm = {
      enable = true;
      wayland = true;
    };
  };
}


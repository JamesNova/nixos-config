{ config, pkgs, lib, inputs, userSettings, ... }:
{
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    inputs.nix-colors.homeManagerModules.default
    ../../home/app/git
    ../../home/app/rofi
    ../../home/app/dunst
    ../../home/shell/zsh
    ../../home/shell/zoxide
    ../../home/app/editor/neovim
    ../../home/desktop/hyprland
    ../../home/ui/waybar
    ../../home/app/browser/${userSettings.browser}.nix
    ../../home/app/term/${userSettings.term}.nix
    ../../home/app/music/ncmpcpp.nix
  ];

  home.username = userSettings.username;
  home.homeDirectory = "/home/${userSettings.username}";

  colorScheme = inputs.nix-colors.colorSchemes.${userSettings.theme};

  home.packages = with pkgs; [
    pfetch
    pavucontrol
    networkmanagerapplet
    htop
    eza
    tigervnc
  ];

  home.sessionVariables = {
  	EDITOR = "nvim";
    TERM = "kitty";
  };

  gtk = {
    enable = true;
    theme.name = "adw-gtk3-dark";
    theme.package = pkgs.adw-gtk3;
    iconTheme.name = "Papirus-Dark";
    iconTheme.package = pkgs.papirus-icon-theme;
    cursorTheme.name = "Bibata-Modern-Classic";
    cursorTheme.package = pkgs.bibata-cursors;
  };

  # targets.genericLinux.enable = true; #uncomment this in non-nixos distributions
  programs.home-manager.enable = true;
  home.stateVersion = "23.11";
}
